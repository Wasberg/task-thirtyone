import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../../../Models/user.model';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {

  @Input() formTitle: string;

  @Output() registerAttempt: EventEmitter<any> = new EventEmitter();

  // defines user in user.model
  user = {} as User

  confirmPassword = '';

  // private messages
  private registerResult: any = {
    message: ''
  };

  constructor(private authService: AuthService, private router: Router) { }

  async onRegisterClicked() {

    // checks if passwords match
    if (this.user.password === this.confirmPassword) {

      try {
        //Register succeded
        this.registerResult.message = "You are now registered.";

        const result: any = await this.authService.register(this.user);

        console.log(result);

      } catch (e) {
        console.error(e);
        // Shows you what problem you encountered and resets from
        this.registerResult.message = "You encountered a problem creating your account: " + e.error.error;
        this.user.username = '';
        this.user.password = '';
        this.confirmPassword = '';
      }

    } else {
      // if password doesn't match.
      this.registerResult.message = "Your passwords doesn't match.";
    }

    // sends outcome to alert.
    this.registerAttempt.emit(this.registerResult);

  }



  ngOnInit(): void {
  }

}
