import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { User } from 'src/app/Models/user.model';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  @Input() formTitle: string;

  @Output() loginAttempt: EventEmitter<any> = new EventEmitter();

  //defines user as in user model.
  user = {} as User

  private loginResult: any = {
    isLoggedIn: false,
    message: ''
  };

  constructor(private authService: AuthService) { }

  async onLoginClicked() {


    if (this.loginResult.isLoggedIn === false) {
      // tries connection to API
      try {

        const result = await this.authService.login(this.user);

        this.loginResult.message = "Congratulations you are now logged in.";
        this.loginResult.isLoggedIn = true;

        console.log(result);

      } catch (e) {
        // Shows you what went wrong and reset form.
        console.error(e)

        this.loginResult.message = "Something went wrong: " + e.error.error;

        this.user.username = '';
        this.user.password = '';
      }

    } else {
      // If you are already logged in.
      this.loginResult.message = "You are already logged in."
    }

    this.loginAttempt.emit(this.loginResult);
  }



  ngOnInit(): void {
  }

}
